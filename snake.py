import curses
import time
import os
from pynput import keyboard
from threading import Thread
from colorama import Fore, Back, Style, init
import random

# config #

def reload():
    global x, y, direction, long, savex, savey, eat, score, print_score

    x = 5 # start x
    y = 5 # start y
    direction = 'down' # start direction
    long = 2 # start snake length

    savex = []
    savey = []
    eat = False
    score = 0
    print_score = ''

timesleep = 0.1 # speed
snake = "0" # snake simbol
head = "Q" # snake head
ripsnake = "X" # rip snake
eat_simbol = "#" # eat
best = "0"

# end #

os.system("print '\e[4 q'")
os.system('clear')
reload()
init()
screen = curses.initscr()

curses.start_color()
curses.init_pair(1, curses.COLOR_GREEN, curses.COLOR_BLACK)
curses.init_pair(2, curses.COLOR_RED, curses.COLOR_BLACK)
curses.init_pair(3, curses.COLOR_YELLOW, curses.COLOR_BLACK)

num_rows, num_cols = screen.getmaxyx()
curses.endwin()
# print(num_rows)
# print(num_cols)
# time.sleep(5)


def print_center(message, color):
    # Calculate center row
    middle_row = int(num_rows / 2)

    # Calculate center column, and then adjust starting position based
    # on the length of the message
    half_length_of_message = int(len(message) / 2)
    middle_column = int(num_cols / 2)
    x_position = middle_column - half_length_of_message

    screen.addstr(middle_row, x_position, message, curses.color_pair(color))
    screen.refresh()


def change_direction(key):
    global direction

    match key:
        case keyboard.Key.down:
            direction = 'down'
        case keyboard.Key.up:
            direction = 'up'
        case keyboard.Key.right:
            direction = 'right'
        case keyboard.Key.left:
            direction = 'left'
        case "-":
            direction = 'stop'
        # case _:
        #     print("404")


listener = keyboard.Listener(
    on_press=change_direction
)
listener.start()


while True:
    rip = False
    time.sleep(timesleep)

    if not eat:
        xeat = random.randint(15, num_rows)
        yeat = random.randint(2, num_cols)
        eat = True
    try: # хз
        screen.addstr(xeat, yeat, eat_simbol, curses.color_pair(3))
    except:
        eat = False


    match direction:
        case 'down':
            y += 1
        case 'up':
            y -= 1
        case 'left':
            x -= 1
        case 'right':
            x += 1
        case "stop":
            pass
        case _:
            print("404")

    try:
        screen.addstr(y, x, head, curses.color_pair(1))
    except:
        rip = True

    num = -1

    try:
        for i in range(long):
            if y == savey[num] and x == savex[num]:
                rip = True
                break
            num -= 1
    except:
        pass

    num = -1

    try:
        for i in range(long):

            if rip:
                screen.addstr(savey[num], savex[num], ripsnake, curses.color_pair(2))
            else:
                screen.addstr(savey[num], savex[num], snake, curses.color_pair(1))
            num -= 1
    except IndexError:
        pass

    if y == xeat and x == yeat:
        eat = False
        long += 1
        score += 1
    if best != "0":
        print_score = "Score: " + str(score) + "   Best: " + str(best)
    else:
        print_score = "Score: " + str(score)
    screen.addstr(0, 0, print_score, curses.color_pair(3))


    screen.refresh()
    screen.clear()

    if rip:
        time.sleep(0.5)
        print_center("WASTED!", 2)
        time.sleep(1)
        screen.clear()

        if score > int(best):
            best = score


        print_center(print_score, 3)
        time.sleep(2)
        screen.clear()

        reload()
        count = 4
        for i in range(3):
            count -= 1
            print_center(str(count), 2)
            time.sleep(0.3)
        print_center("Go!", 1)

        continue


    savex.append(x)
    savey.append(y)
